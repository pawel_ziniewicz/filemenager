import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Baza {

	private File file;

	// private String[] pathDirectory;
	//private String[] pathsFile;

	private File[] listaKatalogow;
	//private String fileName;

	// private File[] listaPlikow;

	public Baza(){
	file = new File(ClassLoader.getSystemClassLoader().getResource("").getPath());
	fillPath();
	}
	
//	public void setFile(String sciezka) {
//
//		try {
//			file = new File(sciezka);
//			fillPath();
//		} catch (Exception e) {
//			// if any error occurs
//			e.printStackTrace();
//		}
//		
//	}

	private void fillPath() {
		//pathsFile = file.list();
		listaKatalogow=file.listFiles();
	}

	public void printFile() {
		for (File path : listaKatalogow) {
			if (path.isFile()){
			System.out.println(path.getName());
			}
			else if (path.isDirectory()){
			System.out.println("<D> "+path.getName());
			}
			
			
		}
	}
	
    public void load(String plik) {
        String text;
        File temp= new File(file.getAbsolutePath() + "\\"+plik);
        try(BufferedReader reader = new BufferedReader(new FileReader(temp))) {
            //text = reader.readLine();
            
            
            while((text = reader.readLine()) != null)
            {
                System.out.println(text);
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
       // return text;
    }
    
    public void cd(BufferedReader reader) throws IOException{
    	
    	String plik = reader.readLine();
    	File temp= new File(file.getAbsolutePath() + "\\"+plik);
    	file=temp;
    	fillPath();
    //	System.out.println(file.getAbsolutePath());
    }
    
    public void back(){
    	file=file.getParentFile();
    	fillPath();
    	//System.out.println(file.getAbsolutePath());
    }

    public void printPath(){
       	System.out.println(file.getAbsolutePath());
    }

}
